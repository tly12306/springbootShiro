package com.wpzy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.wpzy.mapper")
public class WpzySpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(WpzySpringBootApplication.class,args);
    }
}
