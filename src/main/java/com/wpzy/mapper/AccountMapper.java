package com.wpzy.mapper;


import com.wpzy.entity.Perms;

import com.wpzy.entity.Role;
import com.wpzy.entity.User;

import java.util.List;

/**
 * @author admin
 */
public interface AccountMapper{

    User findRolesByUserName( String username);

    Role findPermsByRoleId(String roleId);

    User findByUserName( String username);
}
