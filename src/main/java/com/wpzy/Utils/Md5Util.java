package com.wpzy.Utils;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.stereotype.Component;

/**
 * @author admin
 */
@Component
public class Md5Util {
    private static int hashInterations=1024;
    public static String md5(String source,String salt){
        return new Md5Hash (source,salt,hashInterations).toString();
    }
    public static String md5_salt(String source,String salt){
        return new Md5Hash(source,salt,hashInterations).toString();
    }
}
