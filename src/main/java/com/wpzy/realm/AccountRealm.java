package com.wpzy.realm;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.wpzy.Utils.ApplicationContextUtil;
import com.wpzy.entity.Perms;
import com.wpzy.entity.Role;
import com.wpzy.entity.User;
import com.wpzy.salt.MyByteSource;
import com.wpzy.service.AccountService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;

import java.util.List;


/**
 * @author admin
 */
public class AccountRealm extends AuthorizingRealm {

    /**
     * 角色的权限集合，授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取身份信息
        User myUser = (User) SecurityUtils.getSubject().getPrincipal();
        //account包含数据库中的用户密码
        AccountService accountService = (AccountService) ApplicationContextUtil.getBean("accountService");
        User user = accountService.findRolesByUserName(myUser.getUsername());
        //授权角色信息
            SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
            user.getRoles().forEach(role->{
                simpleAuthorizationInfo.addRole(role.getRoleName());
                //权限信息
                Role roles = accountService.findPermsByRoleId(role.getRoleId());
                    roles.getPerms().forEach(perm->{
                        simpleAuthorizationInfo.addStringPermission(perm.getPerms());
                    });
            });
            return simpleAuthorizationInfo;
    }

    /**
     * 用户的角色申请、认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //authenticationToken中本身已包含用户名、密码（客户端传过来的）
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        //account包含数据库中的用户密码
        AccountService accountService = (AccountService) ApplicationContextUtil.getBean("accountService");
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        User user =accountService.findByUsername(token.getUsername());
        session.setAttribute("account", user);
        if (user != null) {
            return new SimpleAuthenticationInfo(user, user.getPassword(),new MyByteSource(user.getSalt()), getName());
        }
        return null;
    }
}
