package com.wpzy.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import com.wpzy.realm.AccountRealm;
import com.wpzy.config.redisCache.RedisCacheManager;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @author admin
 */
@Configuration
public class ShiroConfig {
    //配置一个过滤器，可以规定那些请求可以访问哪些不可以
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(@Qualifier("securityManager") DefaultWebSecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        HashMap<String, String> filterChainDefinitionMap = new HashMap<>();
        //验证码
        filterChainDefinitionMap.put("/getImage", "anon");   //anon设置为公共资源
        //权限设置（认证和授权）
        filterChainDefinitionMap.put("/main", "authc");
        filterChainDefinitionMap.put("/manage", "authc");
        filterChainDefinitionMap.put("/administator","authc");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        //设置登录界面
        shiroFilterFactoryBean.setLoginUrl("/login");
        //设置未授权页面
        shiroFilterFactoryBean.setUnauthorizedUrl("/unauth");
        return shiroFilterFactoryBean;
    }

    //配置一个安全管理器
    @Bean
    public DefaultWebSecurityManager securityManager(@Qualifier("accountRealm") AccountRealm accountRealm) {
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(accountRealm);
        // 实现记住我，所需要的配置
        defaultWebSecurityManager.setRememberMeManager(cookieRememberMeManager());
        return defaultWebSecurityManager;
    }

    @Bean
    public AccountRealm accountRealm() {
        AccountRealm accountRealm = new AccountRealm();
        //修改凭证校验匹配器
        HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
        //设置加密算法为md5
        credentialsMatcher.setHashAlgorithmName("MD5");
        //设置散列次数
        credentialsMatcher.setHashIterations(1024);
        accountRealm.setCredentialsMatcher(credentialsMatcher);
        //redis缓存
        accountRealm.setCacheManager(new RedisCacheManager());
        //开启全局缓存
        accountRealm.setCachingEnabled(true);
        //开启认证缓存
        accountRealm.setAuthenticationCachingEnabled(true);
        //认证缓存名称
        accountRealm.setAuthenticationCacheName("authenticationCache");
        //开启授权缓存
        accountRealm.setAuthorizationCachingEnabled(true);
        //授权缓存名称
        accountRealm.setAuthorizationCacheName("authorizationCache");
        return accountRealm;
    }

    @Bean
    public ShiroDialect shiroDialect(){
        return new ShiroDialect();
    }

    // 实现记住我，所需要的配置
    @Bean
    public CookieRememberMeManager cookieRememberMeManager() {
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCookie(simpleCookie());
        // rememberMe cookie加密的密钥 建议每个项目都不一样 默认AES算法 密钥长度(128 256 512 位)
        cookieRememberMeManager.setCipherKey(Base64.decode("4AvVhmFLUs0KTA3Kprsdag=="));
        return cookieRememberMeManager;
    }
    // 实现记住我，所需要的配置
    @Bean
    public SimpleCookie simpleCookie() {
        // 这个参数是cookie的名称，对应前端的checkbox的name = rememberMe
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
        simpleCookie.setHttpOnly(true);
        // 记住我cookie生效时间1小时，单位秒
        simpleCookie.setMaxAge(60 * 60);
        return simpleCookie;
    }
}
