package com.wpzy.config;


import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

@WebServlet(name = "VertifyCodeServlet",urlPatterns = "/code")
public class VertifyCodeServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int width=100;
        int height=30;
        String data="abcdefg1234560";
        Random random=new Random();
        BufferedImage image=new BufferedImage(width,height,BufferedImage.TYPE_INT_BGR);
        Graphics graphics = image.getGraphics();
        graphics.setColor(Color.gray);
        graphics.fillRect(0,0,width,height);
        graphics.setColor(Color.black);
        for (int i=0;i<4;i++)
        {
            int position = random.nextInt(data.length());
            String randomstr = data.substring(position, position + 1);
            graphics.drawString(randomstr,width/5*(i+1),15);

        }



        ImageIO.write(image,"jpg",response.getOutputStream());
    }
}
