package com.wpzy.controller;

import com.wpzy.Utils.VerifyCodeUtils;
import com.wpzy.service.AccountService;
import org.apache.shiro.SecurityUtils;

import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * @author admin
 */
@Controller
public class AccountController {
    @Autowired
    private AccountService accountService;
    /**
     * @Description: 验证码
     * @Author: 52Hz
     * @Date: 2021/11/4
     * @Time: 14:59
     */
    @RequestMapping("/getImage")
    public void getImage(HttpSession session, HttpServletResponse response) throws IOException {

        //生成验证码
        String code = VerifyCodeUtils.generateVerifyCode(6);
        //验证码存入session
        session.setAttribute("code", code);
        //验证码存入图片
        ServletOutputStream os = response.getOutputStream();
        response.setContentType("image/png");
        VerifyCodeUtils.outputImage(220, 60, os, code);
    }
    @GetMapping("/{url}")
    public String redirect(@PathVariable("url") String url) {
        return url;
    }


    @PostMapping("/login")
    public String login(String username, String password, Model model,String code,Boolean rememberMe,HttpSession session) {
        Subject subject = SecurityUtils.getSubject();
        //封装用户的登陆数据
        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        //比较验证码
        String codes = (String) session.getAttribute("code");
        try {
            if (codes.equalsIgnoreCase(code)){
                //设置记住我
                token.setRememberMe(rememberMe);
                subject.login(token);
                return "index";
            }else{
                throw new RuntimeException("验证码错误!");
            }
        }catch(UnknownAccountException e){
            e.printStackTrace();
            model.addAttribute("msg","用户名错误");
            return "login";
        }catch (IncorrectCredentialsException e){
            e.printStackTrace();
            model.addAttribute("msg","密码错误");
            return "login";
        }
    }
    @GetMapping("/unauth")
    @ResponseBody
    public String unauth() {
        return "您的账号未授权！！！";
    }
    @GetMapping("/logout")
    public String logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "login";
    }
    @GetMapping("/manage")
    @RequiresRoles(value={"manage"})
    @ResponseBody
    public String manage() {
        return "manage";
    }

    @GetMapping("/administator")
    @RequiresRoles(value={"manage"})
    @RequiresPermissions("administator")
    @ResponseBody
    public String administator() {
        return "administator";
    }
}
