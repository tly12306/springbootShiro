package com.wpzy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author admin
 * 角色
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Role implements Serializable {
    private static final long serialVersionUID = 2980628125613624371L;
    private String id;
    private String roleId;
    private String roleName;
    //定义权限的集合
    private List<Perms> perms;
}
