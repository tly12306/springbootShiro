package com.wpzy.service.impl;

import com.wpzy.entity.Perms;
import com.wpzy.entity.Role;
import com.wpzy.entity.User;
import com.wpzy.mapper.AccountMapper;
import com.wpzy.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author admin
 */
@Service("accountService")
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    //根据用户名查询所有角色
    @Override
    public User findRolesByUserName(String username) {
        return accountMapper.findRolesByUserName(username);
    }

    @Override
    public Role findPermsByRoleId(String roleId) {
        return accountMapper.findPermsByRoleId(roleId);
    }

    @Override
    public User findByUsername(String username) {
        return accountMapper.findByUserName(username);
    }
}
