package com.wpzy.service;

import com.wpzy.entity.Perms;
import com.wpzy.entity.Role;
import com.wpzy.entity.User;

import java.util.List;

/**
 * @author admin
 */
public interface AccountService {

    //根据用户名查询所有角色
    User findRolesByUserName(String username);

    Role findPermsByRoleId(String roleId);

    User findByUsername(String username);
}
