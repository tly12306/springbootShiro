package com.wpzy;

import com.wpzy.mapper.AccountMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTest {
    @Autowired
    private AccountMapper accountMapper;

    @Test
    public void findRolesByUserName(){
        System.out.println(accountMapper.findRolesByUserName("ls"));
    }

    @Test
    public void findPermsByRoleId(){
        System.out.println(accountMapper.findPermsByRoleId("101"));
    }
}
